<?php

namespace Drupal\simple_oauth_google_connect\Grant;

use DateInterval;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\simple_oauth\Entities\UserEntity;
use Drupal\social_api\Plugin\NetworkManager;
use Drupal\social_auth\User\UserAuthenticator;
use Drupal\social_auth_google\GoogleAuthManager;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\UserEntityInterface;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\Grant\AbstractGrant;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use League\OAuth2\Server\Repositories\UserRepositoryInterface;
use League\OAuth2\Server\RequestEvent;
use League\OAuth2\Server\ResponseTypes\ResponseTypeInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Facebook grant class.
 */
class GoogleGrant extends AbstractGrant {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\social_auth_google\GoogleAuthManager definition.
   *
   * @var \Drupal\social_auth_google\GoogleAuthManager
   */
  protected $socialAuthGoogleManager;

  /**
   * The network plugin manager.
   *
   * @var \Drupal\social_api\Plugin\NetworkManager
   */
  protected $networkManager;

  /**
   * The Social Auth user authenticator..
   *
   * @var \Drupal\social_auth\User\UserAuthenticator
   */
  protected $userAuthenticator;

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal\Core\Extension\ModuleHandler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    UserRepositoryInterface $userRepository,
    RefreshTokenRepositoryInterface $refreshTokenRepository,
    GoogleAuthManager $googleAuthManager,
    NetworkManager $networkManager,
    UserAuthenticator $userAuthenticator,
    AccountProxyInterface $currentUser,
    ModuleHandler $moduleHandler
  ) {
    $this->setUserRepository($userRepository);
    $this->setRefreshTokenRepository($refreshTokenRepository);
    $this->socialAuthGoogleManager = $googleAuthManager;
    $this->networkManager = $networkManager;
    $this->userAuthenticator = $userAuthenticator;
    $this->currentUser = $currentUser;
    $this->refreshTokenTTL = new DateInterval('P1M');
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public function respondToAccessTokenRequest(
    ServerRequestInterface $request,
    ResponseTypeInterface $responseType,
    DateInterval $accessTokenTTL
  ) {
    $client = $this->validateClient($request);
    $scopes = $this->validateScopes($this->getRequestParameter('scope', $request, $this->defaultScope));
    $user = $this->validateUser($request, $client);

    $finalizedScopes = $this->scopeRepository->finalizeScopes($scopes, $this->getIdentifier(), $client, $user->getIdentifier());

    $accessToken = $this->issueAccessToken($accessTokenTTL, $client, $user->getIdentifier(), $finalizedScopes);
    $this->getEmitter()
      ->emit(new RequestEvent(RequestEvent::ACCESS_TOKEN_ISSUED, $request));
    $responseType->setAccessToken($accessToken);

    $refreshToken = $this->issueRefreshToken($accessToken);

    if ($refreshToken !== NULL) {
      $this->getEmitter()
        ->emit(new RequestEvent(RequestEvent::REFRESH_TOKEN_ISSUED, $request));
      $responseType->setRefreshToken($refreshToken);
    }

    return $responseType;
  }

  /**
   * Validate user by google credentials.
   *
   * @param \Psr\Http\Message\ServerRequestInterface $request
   *   Psr\Http\Message\ServerRequestInterface.
   * @param \League\OAuth2\Server\Entities\ClientEntityInterface $client
   *   League\OAuth2\Server\Entities\ClientEntityInterface.
   *
   * @return \League\OAuth2\Server\Entities\UserEntityInterface
   *   League\OAuth2\Server\Entities\UserEntityInterface.
   *
   * @throws \League\OAuth2\Server\Exception\OAuthServerException
   *   League\OAuth2\Server\Exception\OAuthServerException.
   */
  protected function validateUser(ServerRequestInterface $request, ClientEntityInterface $client) {
    $google_access_token = $this->getRequestParameter('google_access_token', $request);
    $google_token_expires_in = $this->getRequestParameter('google_token_expires_in', $request);

    if (empty($google_access_token)) {
      throw OAuthServerException::invalidRequest('google_access_token');
    }
    if (empty($google_token_expires_in)) {
      throw OAuthServerException::invalidRequest('google_token_expires_in');
    }
    try {
      $this->socialAuthGoogleManager->setAccessToken(new AccessToken([
        'access_token' => $google_access_token,
        'expires_in' => $google_token_expires_in,
      ]));
      /* @var \League\OAuth2\Client\Provider\AbstractProvider|false $client */
      $client = $this->networkManager->createInstance('social_auth_google')
        ->getSdk();
      $this->socialAuthGoogleManager->setClient($client);
      /** @var \League\OAuth2\Client\Provider\GoogleUser $profile */
      $profile = $this->socialAuthGoogleManager->getUserInfo();
      if (empty($profile)) {
        throw OAuthServerException::invalidCredentials();
      }

      $this->userAuthenticator->setPluginId('social_auth_google');
      $data = $this->userAuthenticator->checkProviderIsAssociated($profile->getId()) ? NULL : $this->socialAuthGoogleManager->getExtraDetails();
      $this->userAuthenticator->authenticateUser($profile->getName(),
        $profile->getEmail(),
        $profile->getId(),
        $this->socialAuthGoogleManager->getAccessToken(),
        $profile->getAvatar(), $data);
      $this->moduleHandler->invokeAll('simple_oauth_google_connect_after_login', [$profile]);

    }
    catch (\Exception $exception) {
      throw new OAuthServerException(
        $exception->getMessage(),
        $exception->getCode(),
        'invalid_google_credentials',
        400
      );
    }


    if (!$this->currentUser->isAuthenticated()) {
      throw OAuthServerException::invalidCredentials();
    }
    $user = new UserEntity();
    $user->setIdentifier($this->currentUser->id());
    if ($user instanceof UserEntityInterface === FALSE) {
      $this->getEmitter()
        ->emit(new RequestEvent(RequestEvent::USER_AUTHENTICATION_FAILED, $request));

      throw OAuthServerException::invalidCredentials();
    }

    return $user;
  }

  /**
   * {@inheritdoc}
   */
  public function getIdentifier() {
    return 'google_login_grant';
  }

}
